---
layout: doc
title: Get started
---

Install the current stable version of Ruta:

{% highlight r %}
devtools::install("fdavidcl/ruta")
{% endhighlight %}
