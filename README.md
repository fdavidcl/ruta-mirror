![](https://raw.githubusercontent.com/fdavidcl/ruta/master/docs/images/logo/ruta_logo64.png)

# ruta

[![R language](https://img.shields.io/badge/language-R-lightgrey.svg)](https://www.r-project.org/)
[![Travis](https://img.shields.io/travis/fdavidcl/ruta.svg)](https://travis-ci.org/fdavidcl/ruta)
[![license](https://img.shields.io/github/license/fdavidcl/ruta.svg)](https://www.gnu.org/licenses/gpl.html)

An R package for unsupervised deep architectures.

## Install

```r
devtools::install_github("fdavidcl/ruta")
```
